from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaProjectPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def click_new_project_page(self):
        new_project = self.browser.find_element(By.CSS_SELECTOR, '#content > article > nav > ul > li:nth-child(1) > a')
        new_project.click()

    def verify_new_project_page_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def wait_until_new_project_is_added(self):
        wait = WebDriverWait(self.browser, 3)
        text_area = (By.CSS_SELECTOR, '#j_info_box > p')
        wait.until(expected_conditions.element_to_be_clickable(text_area))

    def verify_new_project_has_been_added(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '#j_info_box > p').text == title

    def click_projects_section(self):
        self.browser.find_element(By.CSS_SELECTOR, '#wrapper > ul > li.item2 > a').click()

    def check_if_new_project_is_on_the_list(self):
        self.browser.find_element(By.CSS_SELECTOR, '#j_searchButton').click()

    def verify_if_project_name_is_on_the_list(self, project_title):
        assert self.browser.find_element(By.CSS_SELECTOR, '#content > article > table > tbody > tr > td:nth-child(1) > a').text == project_title