from selenium.webdriver.common.by import By
class HomePage:
    def __init__(self, browser):
        self.browser = browser

    def verify_post_count(self, expected_count):
        list_of_titles = self.browser.find_elements(By.CSS_SELECTOR, '.post-title')
        assert len(list_of_titles) == expected_count

    def search_for_a_word(self, word):
        search_input = self.browser.find_element(By.CSS_SELECTOR, 'input.gsc-input')
        search_button = self.browser.find_element(By.CSS_SELECTOR, 'input.gsc-search-button')

        search_input.send_keys(word)
        search_button.click()
    def click_label(self, label):
        label_element = self.browser.find_element(By.LINK_TEXT, label)
        label_element.click()




