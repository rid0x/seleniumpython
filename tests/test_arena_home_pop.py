import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager

from pages.test_arena.arena_home import ArenaHomePage
from pages.test_arena.arena_login import ArenaLoginPage
from pages.test_arena.arena_messages import ArenaMessagesPage
from pages.test_arena.arena_project_page import ArenaProjectPage
from pages.test_arena.arena_new_project_page import ArenaNewProjectPage
from utils.random_message import generate_random_text


@pytest.fixture
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()

def test_should_display_email_in_user_section(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.verify_displayed_email('administrator@testarena.pl')

def test_should_successfully_logout(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_logout()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'


def test_should_open_messages_and_display_text_area(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_mail()
    arena_massages_page = ArenaMessagesPage(browser)
    arena_massages_page.wait_for_text_area_load()
    # browser.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    # wait = WebDriverWait(browser, 10)
    # text_area = (By.CSS_SELECTOR, '#j_msgContent')
    # wait.until(expected_conditions.element_to_be_clickable(text_area))


def test_should_open_projects_page(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.verify_title('Projekty')

def test_should_add_new_project(browser):
    arena_home_page = ArenaHomePage(browser)
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.add_new_project()
    arena_new_project_page.verify



